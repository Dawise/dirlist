The Directory Listing Server
 1. When the app in ran, it creates the server listening on port 8080
 2. When the path is passed to the request it return the list of files in the path
 3. The request link will look as follows : http://localhost:8080/dirlist?path=C:/Windows
		Where :
		1. 'http://localhost:8080/' is the link the host
		2. 'dirlist' is the end-point
		3. 'path' is the name of the param
		4. 'C:/Windows' is the path value