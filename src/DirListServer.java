import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirListServer{
	
	private static ServerSocket		m_o_ListeningSocket;

	public static void main(String[] args) {

		// Create a listening socket
		try {
			m_o_ListeningSocket = new ServerSocket(8080);
			
			if (m_o_ListeningSocket != null) {
				System.out.println("Server listening on port 8080...");
			} else {
				System.out.println("Failed to create server on port 8080...");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Failed to create server on port 8080:" + e.getLocalizedMessage());
		}
		
		while(true) {
			sendResponse();
		}

	}
	
	public static void sendResponse() {
		try (Socket o_ClientSocket = m_o_ListeningSocket.accept()) {
			
			String[] str_Listings = readRequest(o_ClientSocket);
			
			// Return Status 200
			String str_HttpResponse = "HTTP/1.1 200 OK\r\n\r\n";
			
			o_ClientSocket.getOutputStream().write(str_HttpResponse.getBytes("UTF-8"));
			
			// Output dir list
			for (String str_Item: str_Listings) {
				if (str_Item != null)
					o_ClientSocket.getOutputStream().write(str_Item.getBytes("UTF-8"));
			}
			
			//o_ClientSocket.getOutputStream().write("");
			
			// Close socket when done
			if (!o_ClientSocket.isClosed()) o_ClientSocket.close();
			
		} catch (IOException e) {
			System.out.println("Error : " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	public static String[] readRequest(Socket p_o_Socket) throws IOException {
		
        BufferedReader in = new BufferedReader(new InputStreamReader(
        		p_o_Socket.getInputStream()));
        
        String str = ".";
        String str_Buffer = "";
        System.out.println("Recieved Data:");
        while (!str.equals("")) {
          str = in.readLine();
          str_Buffer += str;
          System.out.println(str);
        }
        
        if (str_Buffer.indexOf("favicon.ico") > -1)
        	return new String[10];
        
        // Get the path value parameter
        // So the param is as follows ?path=fullpath
        // Where fullpath is the value of path and it contains the directory path
        int i_IndexOfParam = str_Buffer.indexOf("path=") + "path=".length();
        int i_IndexEndOfParam = str_Buffer.indexOf(" ", i_IndexOfParam);
        
        
        System.out.println("i_IndexOfParam = " + i_IndexOfParam);
        System.out.println("i_IndexEndOfParam = " + i_IndexEndOfParam);
        
        
        // Get the list of files in the given 'path'
        String[] str_List = getDirList(str_Buffer.substring(i_IndexOfParam, i_IndexEndOfParam));
        
        return str_List;
	}
	
	public static String[] getDirList(String p_str_Path) {
		System.out.println("p_str_Path = " + p_str_Path);
		
		// Start i_ListCounter at since the first two items is the path and separator line
		int i_ListCounter = 2;
		final int MAX_LIST = 10000000;
		String[] str_List = new String[MAX_LIST + 2];
		
		str_List[0] = p_str_Path + "\n";
		str_List[1] = "----------------------------------------------------------------------------------------\n";
		
		File[] o_Files = new File(p_str_Path).listFiles();
		
		// Ensure the directory is found
		if (o_Files != null) {
		
			for (File o_File: o_Files) {
				
				if (i_ListCounter > MAX_LIST)
					break;
					
				try {
					Path str_Path = Paths.get(o_File.getPath());
					System.out.println("str_Path = " + str_Path);

					str_List[i_ListCounter] = o_File.getName() + "\t\t\t\t\t\t\t" + Files.size(str_Path) + " byte(s)\n";
				} catch (IOException e) {
					System.out.println("Error : " + e.getLocalizedMessage());
					e.printStackTrace();
				}
				
				i_ListCounter++;
				
			}
			
			return str_List;
		} else {
			return new String[] {"Path not found : " + str_List[0]};
		}
		
	}
	
	

}
